#!/bin/bash
#SBATCH -N 1
#SBATCH --partition=batch
#SBATCH -J svdfull
#SBATCH -o svdfull.out
#SBATCH -e svdfull.err
#SBATCH --mail-user=hongyx1993@gmail.com
#SBATCH --mail-type=ALL
#SBATCH --time=24:00:00
#SBATCH --mem=512G
#SBATCH --gres=gpu:1
#SBATCH --nodes=1
#SBATCH --constraint=[v100]

#check run path
echo ${PWD}
export apppath=/home/hongy0a/scratch/gitrepo/svd
if [ "${apppath}" != "${PWD}" ]; then
  echo -e "The Installation Directory:"
  echo -e "==> ${apppath} <=="
  echo -e "is DIFFERENT from your Present Working Direcotry:"
  echo -e "==> ${PWD} <=="
  exit 1
fi
source ${apppath}/module/module_ibex.sh
make clean
make
mkdir -p result
#run the application:
for bs in 256 512 1024 2046 
do
./testing/bin/test_dgesvj_batch --range 32:1024:32 --cuda --nruns 4 --batchCount $bs > result/oldkblassvd_bs$bs.dat
done
