#!/bin/bash 
make clean
source module/module_jasmine.sh
make
for bs in 64 128 256 512 1024
do
./testing/bin/test_dgesvj_batch --range 32:1024:32 --batchCount $bs --nruns 4 -c --cuda > result/$(hostname)bc$bs.dat 
done

