#!/bin/bash
#SBATCH -N 1
#SBATCH --partition=batch
#SBATCH -J LM
#SBATCH -o LM.9wfs.1gpu.out
#SBATCH -e LM.9wfs.1gpu.err
#SBATCH --mail-user=hongyx1993@gmail.com
#SBATCH --mail-type=ALL
#SBATCH --time=00:30:00
#SBATCH --mem=10G
#SBATCH --gres=gpu:1
#SBATCH --nodes=1
#SBATCH --constraint=[v100]


#load module 
module purge
module load cuda/10.1.243
module load gcc/6.4.0
export OPENBLAS_ROOT=/home/hongy0a/sourcecode/OpenBLAS/INSTALL

#check run path
echo ${PWD}
export apppath=/home/hongy0a/sourcecode/Learn_GPU

if [ "${apppath}" != "${PWD}" ]; then
  echo -e "The Installation Directory:"
  echo -e "==> ${apppath} <=="
  echo -e "is DIFFERENT from your Present Working Direcotry:"
  echo -e "==> ${PWD} <=="
  exit 1
fi
# make main
make clean && make main
#run the application:
./main --sys-file=sys-param/sys-param_9wfs_10112Nx.txt --ngpu=1 --saveData --noXY --2pass --profile=eso_test

