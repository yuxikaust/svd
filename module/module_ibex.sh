#!/bin/bash
module purge
module load cuda/9.0.176
module load gcc/6.4.0
module load intel/2018
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/hongy0a/build/lib
mkdir -p lib
mkdir -p src/obj
mkdir -p testing/bin
mkdir -p testing/obj
