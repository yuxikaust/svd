#!/bin/bash

# on master branch
git pull origin master
git add -A
git commit -m 'update script'
git push origin master
# on dev-stable branch
git checkout dev-stable
git pull origin dev-stable
git checkout master script
git checkout master make.inc
git add -A
git commit -m 'update script'
git push origin dev-stable
# on dev-v2 branch
git checkout dev-v2
git pull origin dev-v2
git checkout master script
git checkout master make.inc
git add -A
git commit -m 'update script'
git push origin dev-v2
git checkout master
